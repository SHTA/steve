package com.javacore.steve;

public class Application P{
    static public final String APP_NAME = "Steve";
    static public final String AUTHOR = "Kozenko Alex";
    static public final String VERSION = "0.0.0";

    public static void main(String[] args) {
        System.out.println("Hello, my name is " + APP_NAME);
        System.out.println("My author name is " + AUTHOR);
    }
}