package com.javacore.steve.Command;

import java.util.HashMap;
import java.util.Map;

public enum CommandRegister {
    INSTANCE;

    static Map<String,ACommand> commands;

    static {
        commands = new HashMap<>();
        commands.put("version", new CommandVersion("version"));
        commands.put("version", new CommandVersion("author"));
    }

    public ACommand hasCommand(String name) {
        return commands.containsKey(name);
    }

    public ACommand getCommands(String name) {
        return commands.get(name);
    }
}