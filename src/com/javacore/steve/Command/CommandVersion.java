package com.javacore.steve.Command;

import static com.javacore.steve.Application;

public class CommandVersion extends ACommand {
    public CommandVersion(String name) {
        super(name);
    }

    @Override
    public void execute() {
        System.out.println("My version is " + VERSION);
    }
}
